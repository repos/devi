DEVI
====

devi is a simple Mastodon bot that will repost whatever comes from a
configurable Twitter filtered stream endpoint to a Mastodon account.


INSTALL
-------

git clone https://git.lurk.org/repos/devi.git
cd devi
python3 -m venv venv
source venv/bin/activate
pip3 install tweepy mastodon.py


CONFIGURE
---------

Assuming that you have already your dev account and your app registered on
Twitter, and have your bot account ready and also an app created on Mastodon
linked to your bot account, then you can proceed with the following:

cp config.json.sample config.json

Edit config.json with all your creds and the rule for the filtered stream.
See https://developer.twitter.com/en/docs/twitter-api for more info


RUNNING
-------

cd devi
source venv/bin/activate

While tweaking your rule, better use the following which will preview the
toots without actually posting them to the instance

./devi.py --dry-run

Once you're happy you can just

./devi.py

Hit Ctrl-C to stop it, should quit gracefully and disconnect from Twitter.


LIMITATIONS
-----------

- can only handle one rule of max 512 char, or 1024 if you have academic
  access to the Twitter API
- GIFs and videos URLs are not provided (yet?) in the Twitter API v2 media
  dictionary. It's possible to request them separately but for now I just
  pick the preview image instead to have at least something to display.
- not many (any?) graceful exception handling, I may add some as I
  start encountering them :)


PATCHES
-------

are welcome :)
 via or as emails plz.
  ultrageranium /at/ bleu255 \dot\ com


BPL + DONATIONWARE
------------------

devi is published under the Borges Public License and is donationware.
If you find devi useful and can afford it, a donation to LURK is greatly
appreciated :)

https://opencollective.com/lurk



